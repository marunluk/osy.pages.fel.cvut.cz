---
title: "Přednášky"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Přednášky

## Program

[Video záznamy přednášek z r. 2022][video2022]

|     |      Datum | Téma                                     | Další materiály              | Video 2020             |
|-----|------------|------------------------------------------|------------------------------|------------------------|
|  1. | 2023-09-26 | [Úvod do operačních systémů][l1]         | [Příklady][p1] [(⬇)][p1dl]   | [OSY1][v1]             |
|  2. | 2023-10-03 | [Systémová volání][l2]                   | [Příklady][p2] [(⬇)][p2dl]   | [OSY2][v2]             |
|  3. | 2023-10-10 | [Procesy a vlákna][l3]                   | [Příklady][p3] [(⬇)][p3dl]   | [OSY3][v3]             |
|  4. | 2023-10-17 | [Synchronizace][l4]                      | [Příklady][p4] [(⬇)][p4dl]   | [OSY4][v4]             |
|  5. | 2023-10-24 | [Meziprocesní komunikace][l4]            |                              | [OSY5][v5]             |
|  6. | 2023-10-31 | [Bezpečnost OS][l8]                      |                              | [OSY8][v8]             |
|  7. | 2023-11-07 | [Svobodný/otevřený software][l14]        |                              | [OSY14][v14]           |
|  8. | 2023-11-14 | [Stránkování][l5]                        | [Příklady][p5] [(⬇)][p5dl]   | [OSY6][v6]             |
|  9. | 2023-11-21 | [Správa paměti][l6]                      |                              | [OSY7][v7]             |
| 10. | 2023-11-28 | [Vstup/výstup, ovladače][l9]             |                              | [OSY9][v9]             |
| 11. | 2023-12-05 | [Souborové systémy][l10]                 |                              | [OSY10][v10]           |
| 12. | 2023-12-12 | [Grafický subsystém, HW akcelerace][l11] |                              | [OSY11][v11]           |
| 13. | 2023-12-19 | [Virtualizace][l12]                      | [Příklady][p12] [(⬇)][p12dl] | [OSY12][v12]           |
| 14. | 2024-01-09 | [Mobilní OS (Android)][l13]              |                              | [OSY13][v13]           |


[p1]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/tree/master/examples/01-priklady
[p1dl]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/osy.pages.fel.cvut.cz-master.tar.gz?path=examples/01-priklady
[p2]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/tree/master/examples/02-priklady
[p2dl]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/osy.pages.fel.cvut.cz-master.tar.gz?path=examples/02-priklady
[p3]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/tree/master/examples/03-priklady
[p3dl]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/osy.pages.fel.cvut.cz-master.tar.gz?path=examples/03-priklady
[p4]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/tree/master/examples/04-priklady
[p4dl]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/osy.pages.fel.cvut.cz-master.tar.gz?path=examples/04-priklady
[p5]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/tree/master/examples/05-priklady
[p5dl]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/osy.pages.fel.cvut.cz-master.tar.gz?path=examples/05-priklady
[p12]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/tree/master/examples/kvmtest
[p12dl]: https://gitlab.fel.cvut.cz/osy/osy.pages.fel.cvut.cz/-/archive/master/osy.pages.fel.cvut.cz-master.tar.gz?path=examples/kvmtest

[l1]: pdf/lekce01.pdf
[l2]: pdf/lekce02.pdf
[l3]: pdf/lekce03.pdf
[l4]: pdf/lekce04.pdf
[l5]: pdf/lekce05.pdf
[l6]: pdf/lekce06.pdf
[l7]: pdf/lekce07.pdf
[l8]: pdf/lekce08_security.pdf
[l9]: pdf/lekce09_io.pdf
[l10]: pdf/lekce10_fs.pdf
[l11]: pdf/lekce11_gfx.pdf
[l12]: pdf/lekce12_virt.pdf
[l13]: pdf/lekce13_android.pdf
[l14]: pdf/lekce14_foss.pdf

[v1]: https://www.youtube.com/watch?v=OO2gzo9kK94&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=1
[v2]: https://www.youtube.com/watch?v=iEj0VI-ntLM&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=2
[v3]: https://www.youtube.com/watch?v=7mWEwEISbVU&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=3
[v4]: https://www.youtube.com/watch?v=LjPDmXApN94&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=4
[v5]: https://www.youtube.com/watch?v=QEbuX1LNW7Q&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=5
[v6]: https://www.youtube.com/watch?v=SpExNfx0REw&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=6
[v7]: https://www.youtube.com/watch?v=LVnEXSYgD0M&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=7
[v8]: https://www.youtube.com/watch?v=ap7aBN54g44&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=8
[v9]: https://www.youtube.com/watch?v=q2exF-j_S1A&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=9
[v10]: https://www.youtube.com/watch?v=Og3k31u5D7E&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=10
[v11]: https://www.youtube.com/watch?v=EsxuIcTtNoQ&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=11
[v12]: https://www.youtube.com/watch?v=wNAUV-2ExHQ&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=12
[v13]: https://www.youtube.com/watch?v=UO8f5QF0dpY&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=13
[v14]: https://www.youtube.com/watch?v=CXQelr4cNFQ&list=PLMM7cOQsd8ZKl83y48DNbLMsyfm96azg8&index=14

[video2022]: https://www.youtube.com/playlist?list=PLQL6z4JeTTQmdkrIyMB07vfnDGfEJaRDl

## Aktivita na přednáškách

Na některých přednáškách mohou studenti získat body za aktivitu
odpovědmi na kvízové otázky. Tyto body se započítávají do bodů za
práci v semestru a jsou tedy zastropovány na 65 bodech.
