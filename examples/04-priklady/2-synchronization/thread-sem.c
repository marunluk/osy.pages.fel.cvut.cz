#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int a;
pthread_mutex_t mutex;

void *fce(void *n)
{
    int i;
    for (i = 0; i < 100000; i++) {
        pthread_mutex_lock(&mutex);
        a += 1;
        pthread_mutex_unlock(&mutex);
    }
    printf("a=%i\n", a);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t tid;
    pthread_mutex_init(&mutex, NULL);

    a = 0;
    pthread_create(&tid, NULL, fce, NULL);
    fce(NULL);
    return 0;
}
