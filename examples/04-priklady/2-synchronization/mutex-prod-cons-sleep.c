#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>

char global[8][256];
int glob_free = 8;
int glob_end = 0;
pthread_mutex_t mutex;
pthread_cond_t empty, full;
pthread_t tid_prod, tid_cons;

void sighandler(int arg)
{
    puts("Signal\n");
}

void *producer(void *i)
{
    char str[256], *s;
    int wr_ptr = 0;
    int num;

    for (num = 0; num < 100000; num++) {
        pthread_mutex_lock(&mutex);
        while (glob_free <= 0) {
            pthread_mutex_unlock(&mutex);
            printf("Wait full\n");
            sleep(100);
            pthread_mutex_lock(&mutex);
        }
        sprintf(global[wr_ptr], "Cislo:%i\n", num);
        glob_free--;
        pthread_mutex_unlock(&mutex);
        if (glob_free == 7) {
            pthread_kill(tid_cons, SIGUSR1);
        }
        wr_ptr = (wr_ptr + 1) % 8;
    }
    pthread_mutex_lock(&mutex);
    glob_end = 1;
    pthread_mutex_unlock(&mutex);
    return NULL;
}

void *consumer(void *i)
{
    int rd_ptr = 0;

    pthread_mutex_lock(&mutex);
    while (!glob_end) {
        while (glob_free >= 8 && !glob_end) {
            printf("Wait empty\n");
            pthread_mutex_unlock(&mutex);
            sleep(100);
            pthread_mutex_lock(&mutex);
        }
        printf("Zadano: %s\n", global[rd_ptr]);
        glob_free++;
        pthread_mutex_unlock(&mutex);
        if (glob_free == 1) {
            pthread_kill(tid_prod, SIGUSR1);
        }
        rd_ptr = (rd_ptr + 1) % 8;
        pthread_mutex_lock(&mutex);
    }
    pthread_mutex_unlock(&mutex);
    return NULL;
}

int main(int argc, char *argv[])
{

    struct sigaction sa = {
        .sa_handler = sighandler,
    };
    sigaction(SIGUSR1, &sa, NULL);
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&full, NULL);
    pthread_cond_init(&empty, NULL);

    pthread_create(&tid_cons, NULL, consumer, NULL);
    sleep(1);
    pthread_create(&tid_prod, NULL, producer, NULL);

    pthread_join(tid_prod, NULL);
    pthread_join(tid_cons, NULL);

    printf("Koncim\n");
    return 0;
}
