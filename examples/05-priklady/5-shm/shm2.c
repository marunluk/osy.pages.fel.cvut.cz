#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
    char *shared_mem;
    int fd = shm_open("pamet", O_RDWR | O_CREAT, 0666);
    shared_mem = mmap(NULL, 10240, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    for (int i = 0; i < 1000; i++) {
        printf("Ulozeno: %s", shared_mem);
        shared_mem += 10;
    }
    shm_unlink("pamet");
    return 0;
}
