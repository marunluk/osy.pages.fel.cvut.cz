#include <stdio.h>
#include <unistd.h>

int b()
{
    printf("Co tu delam?\n");
    _exit(1);
}

int main();

void f(int x)
{
    unsigned int local[2];
    int i;
    local[0] = 0x111;
    local[1] = 0x222;
    for (i = 10; i >= 0; i--) {
        printf("%02i - %08x\n", i, local[i]);
    }
    local[6] = ((unsigned int)&b);
    printf("x=%i main %p\n", x, &main);
}

int main()
{
    f(10);
    printf("Proc?\n");
    return 0;
}
